/* globals jQuery */

jQuery(function ($) {
  $('nav').click(function () {
    if ($(window).innerWidth() <= 768) {
      $(this).find('ul').fadeToggle()
    }
  })
})
