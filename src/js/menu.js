/* globals jQuery, $ */

jQuery(function ($) {
  $('.nav-main').on('click', function (e) {
    if ($(window).innerWidth() <= 768) {
      if (!$('nav').hasClass('active')) {
        $(this).find('ul').fadeIn()
        $(this).addClass('active')
      } else if ($('nav').hasClass('active')) {
        $(this).find('ul').fadeOut()
        $(this).removeClass('active')
      }
    }
  })
})
