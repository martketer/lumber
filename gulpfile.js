'use strict'

const gulp = require('gulp')
const browserSync = require('browser-sync').create()
const postcss = require('gulp-postcss')
const sourcemaps = require('gulp-sourcemaps')
const uglify = require('gulp-uglify')
const concat = require('gulp-concat')
const rename = require('gulp-rename')
const imagemin = require('gulp-imagemin')
const del = require('del')

const options = {
  devUrl: 'lumber.dev'
}

gulp.task('css', ['clean'], function () {
  return gulp.src(['./build/postcss/main.css'])
    .pipe(concat('main.css'))
    .pipe(sourcemaps.init())
    .pipe(postcss([
      require('precss'),
      require('autoprefixer'),
      require('lost'),
      require('rucksack-css'),
      require('postcss-custom-media'),
      require('css-mqpacker'),
      require('cssnano')
    ]))
    .pipe(sourcemaps.write('./'))
    .pipe(gulp.dest('./dist/css/'))
    .pipe(browserSync.stream())
})

gulp.task('img', function () {
  return gulp.src([
    './build/img/**/*.*'])
    .pipe(imagemin())
    .pipe(gulp.dest('./dist/img/'))
    .pipe(browserSync.stream())
})

gulp.task('js', function () {
  return gulp.src([
    './build/js/jquery*.js',
    './build/js/menu.js',
    './build/js/main.js'])
    .pipe(sourcemaps.init())
    .pipe(concat('app.js'))
    .pipe(sourcemaps.write('./'))
    .pipe(gulp.dest('./dist/js/'))
    .pipe(browserSync.stream())
})

gulp.task('minify-js', ['clean'], function () {
  return gulp.src('./dist/**/*.js')
    .pipe(sourcemaps.init())
    .pipe(uglify())
    .pipe(rename('app.min.js'))
    .pipe(sourcemaps.write('./'))
    .pipe(gulp.dest('./dist/js/'))
})

gulp.task('clean', function () {
  return del(['./dist/css/**/*.*', './dist/js/**/*.*'])
})

gulp.task('watch', ['css', 'js', 'img'], function () {
  gulp.watch('./build/postcss/**/*.css', ['css'])
  gulp.watch('./build/js/**/*.js', ['js'])
  gulp.watch('./build/img/**/*.*', ['img'])
  gulp.watch(['./views/*.twig', './functions.php']).on('change', browserSync.reload)
})

gulp.task('serve', ['clean', 'watch'], function () {
  browserSync.init({
    proxy: options.devUrl,
    notify: false
  })
})

gulp.task('build', ['css', 'js', 'img'])

gulp.task('default', ['clean', 'build'])
