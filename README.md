
# Lumber Foundational Framework

A foundation for WordPress theme development built ontop of [Timber](//upstatement.com/timber).

## Installation

### Via VV
1. Ensure `https://bitbucket.org/martketer_cole/lumber/get/HEAD.zip` is your theme location
2. Ensure the `timber-library` plugin is installed and activated

### Via GitHub
1. Navigate to the `themes` directory
2. `git clone https://martketer_cole@bitbucket.org/martketer_cole/lumber.git`
3. Remove unneeded git files `cd lumber && rm -rf .git*`
